package com.jawa.onboardingscreen;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

//TODO: 8. create slider adapter class and write code on below
public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    //Arrays
    public int slide_images[] =
            {
                    R.drawable.eat,
                    R.drawable.owel,
                    R.drawable.admin
            };
    public String[] slide_headings = {
            "EAT",
            "SLEEP",
            "CODE"
    };
    public int[] slide_descs = {
            R.string.lipsum,
            R.string.lipsum,
            R.string.lipsum
    };

    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (RelativeLayout) object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView imageView = view.findViewById(R.id.img);
        TextView textViewHeader = view.findViewById(R.id.textHeader);
        TextView textViewDecs = view.findViewById(R.id.textDecs);

        imageView.setImageResource(slide_images[position]);
        textViewHeader.setText(slide_headings[position]);
        textViewDecs.setText(slide_descs[position]);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }
}
