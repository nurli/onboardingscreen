package com.jawa.onboardingscreen;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private LinearLayout linearLayout1;
    //TODO: 9. create variable object of class sliderAdapter
    private SliderAdapter sliderAdapter;

    private int uiImmersiveOptions = 0;
    private Display display;
    private Point size;
    private int sWidth = 0;
    private int sHeight = 0;
    private int device_height = 0;
    private int device_width = 0;
    private View decorView;

    //TODO: 13. create array of Dot
    private TextView[] mDots;
    //TODO: 21. make variable of button back and next
    private Button btnBack, btnNext;
    //TODO: 22. make currentPage variable
    private int currentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.viewPager);
        linearLayout1 = findViewById(R.id.linear1);
        btnBack = findViewById(R.id.btnBack);
        btnNext = findViewById(R.id.btnNext);

        //TODO: 12. set Fullscreen
        Display windowManager = getWindowManager().getDefaultDisplay();
        display = windowManager;
        size = new Point();
        display.getSize(size);
        sWidth = size.x;
        sHeight = size.y;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getMetrics(displayMetrics);
        device_height = displayMetrics.heightPixels;
        device_width = displayMetrics.widthPixels;
        uiImmersiveOptions = (View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(uiImmersiveOptions);

        //TODO: 10. Initialize class Adapter
        sliderAdapter = new SliderAdapter(this);
        //TODO: 11. setAdapter to viewPager
        viewPager.setAdapter(sliderAdapter);

        //TODO: 16. call addDotsIndicator methode
        addDotsIndicator(0);
        //TODO 19. call addOnPageChangeListener for viewPager
        viewPager.addOnPageChangeListener(viewListener);
        //TODO: 25. direct to another activity with btnNext when condition text change to "Finish"
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnNext.getText().toString().trim().equals("Finish")) {
                    Intent intent = new Intent(MainActivity.this, DashActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    viewPager.setCurrentItem(currentPage + 1);
                }
            }
        });
        //TODO: 26. change to div currentPage
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(currentPage - 1);
            }
        });
    }

    //TODO: 14. make methode addDotsIndicator
    public void addDotsIndicator(int position) {
        //TODO: 15. make summary of dot, i use 3 of dots for this slide and looping it
        mDots = new TextView[3];
        //TODO: 20. call removeAllView of linearlayout for remove the looping of dots summary when swipe
        linearLayout1.removeAllViews();

        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(getResources().getColor(R.color.colorTransparentWhite));

            linearLayout1.addView(mDots[i]);
        }

        if (mDots.length > 0) {
            mDots[position].setTextColor(getResources().getColor(R.color.colorWhite));
        }
    }

    //TODO: 17. create object of pageChangelister view pager on swipe the sliders
    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            //TODO: 18. call the methode of addDotsIndicator
            addDotsIndicator(position);
            //TODO: 23. set currentPage value of position
            currentPage = position;
            //TODO: 24. make condition of position
            if (position == 0) {
                btnNext.setEnabled(true);
                btnBack.setEnabled(false);
                btnBack.setVisibility(View.INVISIBLE);

                btnNext.setText("Next");
                btnBack.setText("");
            } else if (position == mDots.length - 1) {
                btnNext.setEnabled(true);
                btnBack.setEnabled(true);
                btnBack.setVisibility(View.VISIBLE);

                btnNext.setText("Finish");
                btnBack.setText("Back");
            } else {
                btnNext.setEnabled(true);
                btnBack.setEnabled(true);
                btnBack.setVisibility(View.VISIBLE);

                btnNext.setText("Next");
                btnBack.setText("Back");
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
